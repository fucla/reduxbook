export function selectBook(book) {
  /**
   * selectBook is an ActionCreator, it needs to return an action, an object
   * with a type property. Actions are payloads.
   */
  return {
    type: 'BOOK_SELECTED', // usually not a string
    payload: book,
  };
}
