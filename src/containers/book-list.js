import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectBook } from '../actions/index';
import { bindActionCreators } from 'redux';

/**
 * Container is a React component with access to state in Redux.
 * Use React-Redux to make connection.
 */
class BookList extends Component {
  renderList() {
    return this.props.books.map((book) => { // eslint-disable-line
      return (
        <li
          key={ book.title }
          onClick={ () => this.props.selectBook(book) }
          className="list-group-item"
        >
          { book.title }
        </li>
      );
    });
  }

  render() {
    return (
      <ul className="list-group col-sm-4">
        { this.renderList() }
      </ul>
    );
  }
}

BookList.propTypes = {
  books: React.PropTypes.array,
  selectBook: React.PropTypes.func,
};

function mapStateToProps(state) {
  /**
   * Whatever is returned will show up as props inside of BookList.
   * If state changes, container re-renders.
   */
  return {
    books: state.books,
  };
}

/**
 * Anything returned from this function will end up as props on the
 * Booklist container.
 */
function mapDispatchToProps(dispatch) {
  /**
   * Whenever selectBook is called, result should be passed to
   * all our reducers. dispatch is a function.
   */
  return bindActionCreators({ selectBook }, dispatch);
}

/**
 * Promote BookList from component to container - it nees to know about
 * this new dispatch method, selectBook. Make it available as prop.
 */
export default connect(mapStateToProps, mapDispatchToProps)(BookList);
