/**
 * Reducers always have two arguments. State argument is not application state,
 * only the state this reducer is responsible for. Object returned by reducer
 * must be new. Do not mutate.
 */
export default function (state = null, action) { // if state undefined, is null
  switch (action.type) {
    case 'BOOK_SELECTED':
      return action.payload;
    default:
      return state; // redux does not allow reducer to return undefined
  }
}
